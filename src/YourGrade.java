import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) {
        String strScore;
        int score;
        String grade = "Unknow";
        Scanner sc = new Scanner(System.in);
        System.out.print("Print input your score: ");  
        strScore = sc.next();
        score = Integer.parseInt(strScore);

        if(score>=80){
            grade = "A";
        } else if(score>=75){
            grade = "B+";
        } else if(score>=65){
            grade = "B";
        } else if(score>=60){
            grade = "C+";
        } else if(score>=55){
            grade = "C";
        }
        else {
            grade = "F";
        }
        System.out.println(grade);
    }
}
